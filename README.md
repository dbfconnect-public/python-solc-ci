python-solc-ci
==============

A Docker image with Python, tools to build native extensions
and Ethereum `solc`, the Solidity compiler.

The image is used to test smart contracts using cucumber and
[`web3.py`](https://github.com/ethereum/web3.py) in
Continuous Integration.
